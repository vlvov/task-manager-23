package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IRepository;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

}
