package ru.t1.vlvov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    void clear();

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    @Nullable
    List<M> findAll(@NotNull Comparator comparator);

}
